import React from "react";
import { View, TextInput, Image, SafeAreaView } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";

const TextComponent = (props) => {
  return (
    <View style={{ flex: 1 }}>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          borderWidth: 1,
          
          height: 52,
          marginLeft: 38,
          marginRight: 38,
          borderRadius: 5,
          borderColor: "#000000",
          backgroundColor: "#ffffff",
          shadowColor: "#000000",
          shadowOffset: {
            width: 104,
            height: 10,
          },
          shadowOpacity: 1,
          shadowRadius: 5,
          elevation:10
        }}
      >
        <View style={{ flex: 1 }}>
          <TextInput
            secureTextEntry={props.TextType}
            onChange={(text) => {
              props.onType(text);
            }}
            placeholder={props.text}
            placeholderTextColor='rgba(0,0,0,0.33)'
            color='red'
            
            style={{
              color: "#000000",
              margin: 0,
              marginLeft: 27,
              letterSpacing: 0.2,
              // backgroundColor: 'red',
               borderColor: 'black',
              fontFamily: "Nunito",
              fontStyle: "normal",
              fontWeight: "600",
              fontSize: 11,

              // color: "rgba(0,0,0,0.33)",
              height: 50,
            }}
          ></TextInput>
        </View>

        <Image
          source={props.image}
          width={20}
          height={7}
          style={{
            marginLeft: 18,
            marginRight: 18,
            tintColor: "#676767",
            marginTop: 10,
          }}
        />
      </View>
    </View>
  );
};

export default TextComponent;
