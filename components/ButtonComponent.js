import React from "react";
import { View, TextInput, Text, Image,TouchableOpacity, SafeAreaView } from "react-native";
// import { TouchableOpacity } from "react-native-gesture-handler";

const ButtonComponent = (props) => {
  // console.log("values==", props.value);
  return (
    // <SafeAreaView style={{ flex: 1 }}>
    //  <TouchableOpacity onPress={props.onClick}>

      <View
        style={{
          // flexDirection: "row",
          // justifyContent: "space-between",
          justifyContent:'center',
          borderWidth: 1,
          height: 44,
          marginLeft: 60,
          marginRight: 60,
          borderRadius: 5,
          borderColor: "grey",
          color: "rgba(0,0,0,0.37)",
          backgroundColor: props.color?props.color:"#fff",
          shadowColor: "##000000",
          shadowOffset: {
            width: 104,
            height: 10,
          },
          shadowOpacity: 1,
          shadowRadius: 5,
          elevation:10
        }}
      >
        <Text style={{width:'100%',fontFamily:'Nunito',fontStyle:'normal',fontSize:14,fontWeight:'600',letterSpacing:2,textAlign:'center',justifyContent:'center',alignSelf:'center',color:props.color?'#fff':'#000'}}>{props.textValue}</Text>
      </View>
      // </TouchableOpacity>

    // </SafeAreaView>
  );
};

export default ButtonComponent;
