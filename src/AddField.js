import React from 'react';
import {Text, View, TextInput, keyType, Image} from 'react-native';
const AddField = (props) => {
  return (
    <View style={{marginLeft: 16, marginTop: 5, marginRight: 16}}>
      <TextInput
        {...props}
        value={props.value}
        onChangeText={props.onChangeText}
        placeholderTextColor="#fff"
        placeholder={props.placeholder}
        autoCompleteType="name"
        secureTextEntry={props.secureTextEntry}
        keyboardType={props.keyType ? props.keyType : 'visible-password'}
        style={{
          backgroundColor: '#100c08',
          height: 40,
          color: '#fff',
          fontSize: 16,
          lineHeight: 27,
          letterSpacing: 1.4,
          borderRadius: 5,
        }}></TextInput>
    </View>
  );
};

export default AddField;
