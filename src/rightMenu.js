import React, {Component} from 'react';
import {
  View,
  TouchableOpacity,
  Image,
  Modal,
  Text,
  Button,
  Alert,
} from 'react-native';
import AddField from './AddField';
import CalenderComponent from './calenderComponent';

const Rightmenu = ({
  modalVisible,
  hour,
  handleHour,
  minute,
  handleminute,
  handleModel,
  today,
  handleSetDate,
  handleDay
}) => {
  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={modalVisible}
      style={{backgroundColor: '#06566e'}}
      onRequestClose={() => {}}>
      <View
        style={{
          marginTop: 50,
          marginLeft: 50,
          backgroundColor: 'white',
          marginBottom: 60,
          flex: 1,
          flexDirection: 'column',
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            // width: 150,
          }}>
          <View style={{width: 100}}>
            <Text style={{color: '#000', marginLeft: 16}}>Hours</Text>
            <AddField
              secureTextEntry={false}
              placeholder="00"
              keyType="decimal-pad"
              value={hour}
              onChangeText={handleHour}
            />
          </View>
          <View style={{width: 100}}>
            <Text style={{color: '#000', marginLeft: 16}}>Minute</Text>
            <AddField
              value={minute}
              onChangeText={handleminute}
              secureTextEntry={false}
              placeholder="00"
              keyType="decimal-pad"
            />
          </View>
        </View>

        <View style={{marginTop: 20, padding: 10}}>
          <CalenderComponent
            handleDay={handleDay}
          />
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
          <TouchableOpacity
            onPress={handleSetDate}
            style={{
              backgroundColor: 'green',
              height: 40,
              width: 120,
              alignSelf: 'center',
              borderRadius: 10,
              justifyContent: 'center',
            }}>
            <Text style={{fontSize: 20, textAlign: 'center', color: 'white'}}>
              Set Date
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => handleModel(false)}
            style={{
              backgroundColor: 'red',
              height: 35,
              borderRadius: 10,
              width: 40,
              justifyContent: 'center',
            }}>
            <Image
              source={require('../images/close.png')}
              style={{height: 25, width: 25, alignSelf: 'center'}}></Image>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

export default Rightmenu;
