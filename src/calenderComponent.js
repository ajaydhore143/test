import React from 'react';
import {Calendar, CalendarList, Agenda} from 'react-native-calendars';
import {LocaleConfig} from 'react-native-calendars';

const CalendarComponent = ({handleDay}) => {
  return (
    <Calendar
      // onDayPress={(day) => {
      //   console.log('selected day', day);
      // }}
      onDayPress={(day)=>handleDay(day)}
    />
  );
};

export default CalendarComponent;
