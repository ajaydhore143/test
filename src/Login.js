import React, {useState} from 'react';
import {View, TouchableOpacity, Image, Text} from 'react-native';
import CircleTimer from './CircleTimer';
import AddField from './AddField';
import LeftMenu from './leftMenu';
import Rightmenu from './rightMenu';
const HomeScreen = () => {
  const [modalVisible, setModalVisible] = useState(false);
  const [modalVisible1, setModalVisible1] = useState(false);
  const [time, setTime] = useState('');
  const [task, setTask] = useState('');
  const [hour, setHour] = useState();
  const [minute, setMinute] = useState();
  const [today, setToday] = useState([]);
  const [seconds, setSeconds] = useState(1800);

  return (
    <>
      <LeftMenu
        modalVisible1={modalVisible1}
        today={today}
        handleModel={() => setModalVisible1(false)}
        handleSecondClick={(data) => {
          console.log('daaaaaa', data);
          setSeconds(data.worktime);
        }}
      />
      <Rightmenu
        minute={minute}
        hour={hour}
        modalVisible={modalVisible}
        handleModel={() => setModalVisible(false)}
        handleminute={(Text) => {
          if (parseInt(Text) < 61) setMinute(Text);
        }}
        handleHour={(Text) => {
          if (parseInt(Text) < 13) setHour(Text);
        }}
        handleSetDate={() => {
          let temp = [];
          temp = today;
          setSeconds(parseInt(hour) * 60 * 60 + parseInt(minute) * 60);
          temp.push({
            worktime: parseInt(hour) * 60 * 60 + parseInt(minute) * 60,
            task: task,
            date: time,
          });
          setToday(temp);
          setHour('');
          setMinute('');
          setTask('');
        }}
        handleDay={(day) => {
          setTime(day.dateString);
        }}
      />

      <View
        style={{
          backgroundColor: '#06566e',
          flex: 1,
        }}>
        <View
          style={{
            flexDirection: 'row',
            margin: 16,
            justifyContent: 'space-between',
          }}>
          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity onPress={() => setModalVisible1(true)}>
              <Image
                source={require('../images/leftMenu.png')}
                style={{height: 25, width: 25}}></Image>
            </TouchableOpacity>
            <Text
              style={{
                marginLeft: 16,
                fontSize: 20,
                color: '#fff',
                alignItems: 'baseline',
              }}>
              Mike
            </Text>
          </View>
          <Image
            source={require('../images/rightMenu.png')}
            style={{height: 25, width: 25}}></Image>
        </View>
        <AddField
          onChangeText={(Text) => {
            console.log('hello text', task);
            setTask(Text);
          }}
          placeholder="Type Task Name"
        />
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            flex: 1,
          }}>
          <CircleTimer
            radius={120}
            borderWidth={5}
            timer={seconds}
            seconds={1800}
            borderColor={'#F5F5F5'}
            borderBackgroundColor={'#FF0000'}
            onTimeElapsed={() => {
              console.log('Timer Finished!');
            }}
            showSecond={true}></CircleTimer>
        </View>
        <TouchableOpacity
          onPress={() => setModalVisible(true)}
          style={{
            backgroundColor: '#100c08',
            height: 40,
            color: '#000',
            letterSpacing: 1.4,
            margin: 16,
            borderRadius: 5,
            // margin: 0,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <Text style={{marginLeft: 10, fontSize: 18, color: '#fff'}}>
            Add Time
          </Text>
          <Image
            source={require('../images/calender.png')}
            style={{height: 20, width: 20, marginRight: 10}}
          />
        </TouchableOpacity>
      </View>
    </>
  );
};

export default HomeScreen;
