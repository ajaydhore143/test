import React, {Component} from 'react';
import {
  View,
  TouchableOpacity,
  Image,
  Modal,
  Text,
  Button,
  Alert,
} from 'react-native';
const LeftMenu = ({handleSecondClick,modalVisible1, handleModel, today}) => {
  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={modalVisible1}
      style={{backgroundColor: 'red'}}
      onRequestClose={() => {
        // Alert.alert('Modal has been closed.');
      }}>
      <View
        style={{
          marginTop: 50,
          marginLeft: 50,
          backgroundColor: 'white',
          marginBottom: 60,
          flex: 1,
          flexDirection: 'column',
        }}>
        <View
          style={{
            flexDirection: 'row',
            margin: 16,
            justifyContent: 'space-between',
          }}>
          <View style={{flexDirection: 'row'}}>
            <Image
              source={require('../images/leftMenu.png')}
              style={{height: 25, width: 25, tintColor: '#000'}}></Image>

            <Text
              style={{
                marginLeft: 16,
                fontSize: 20,
                color: '#000',
                alignItems: 'baseline',
              }}>
              Mike
            </Text>
          </View>
          <TouchableOpacity onPress={handleModel}>
            <Image
              source={require('../images/back.png')}
              style={{height: 25, width: 25}}></Image>
          </TouchableOpacity>
        </View>
        <Text
          style={{
            color: '#000',
            marginLeft: 10,
            fontSize: 18,
          }}>
          Todays Task
        </Text>

        {today.map((data, index) => {
          return (
            <TouchableOpacity key={index} onPress={()=>handleSecondClick(data)} style={{flexDirection: 'row', marginTop: 5}}>
              <View
                style={{
                  backgroundColor: 'green',
                  height: 10,
                  width: 10,
                  borderRadius: 50,
                  alignSelf: 'center',
                  marginLeft: 5,
                }}></View>
              <Text style={{color: '#000', marginLeft: 10, fontSize: 14}}>
                {data.task}
              </Text>
            </TouchableOpacity>
          );
        })}
      </View>
    </Modal>
  );
};

export default LeftMenu;
